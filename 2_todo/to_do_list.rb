class ToDoList
	attr_accessor :task_name, :index, :pending, :finished

	def initialize
		@task_name = task_name
		@pending = []
		@finished = []
	end

	def add_task(task_name)
		puts "#{task_name} has been added to the your task list."
		@pending.push(task_name)
	end

	def view_pending_tasks
		puts "Pending Tasks"
		a= pending
		a.each {|x| puts x }
	end

	def accomplish_task(index)
		puts "#{pending[index]} has been marked as accomplished."
		@finished.push(pending[index])
		pending.delete_at(index)
	end

	def view_accomplished_tasks
		puts "Accomplished Tasks"
		a = finished
		a.each {|x| puts  x}

	end
end

require './to_do_list'

todo = ToDoList.new

puts "================ \n"

todo.add_task("Clean room")
todo.add_task("Submit MIS21 HW")
todo.add_task("Study for MIS21 midterms")
todo.add_task("Study for Stat midterms")
todo.add_task("Submit advisement form")
todo.add_task("Consult for final project")

puts "================ \n"

todo.view_pending_tasks

puts "================ \n"

todo.accomplish_task(0)
todo.accomplish_task(2)
todo.accomplish_task(2)

puts "================ \n"

todo.view_accomplished_tasks


