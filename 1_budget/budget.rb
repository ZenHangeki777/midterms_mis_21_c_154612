class Budget
	attr_accessor :balance, :item, :amount, :date, :month, :year, :income, :expenses
	def initialize (balance)
		@balance=balance
		@item=item
		@amount=amount
		@date=date
		@month=month
		@year=year
		@income=income
		@expenses=expenses
		puts "Initial Balance: #{balance}0"
	end

	def add_expense(item,amount,date)
		@balance= balance - amount
		@expenses =+ amount
		puts "#{item}: An amount of #{amount}0 was deducted from the balance on #{date}."

	end

	def add_income(item,amount,date)
		@balance= balance + amount
		@income =+ amount
		puts "#{item}: An amount of #{amount}0 was added to the balance on #{date}."
	end

	def monthly_report(month,year)
		puts 
			"=====================================
			  MONTHLY REPORT: #{month} #{year}
			  Total Expense: #{expenses}0
			  Total Income: #{income}0
			====================================="
	end

	def current
		puts "Balance: #{balance}0"
	end
end

require './budget'

budget = Budget.new(50000.00)
budget.add_expense("Petron", 5000.00, "2017-06-15")
budget.add_income("Work", 6000.00, "2017-06-26")
budget.add_expense("National Bookstore", 2500.00, "2017-06-27")
budget.add_expense("Gonzaga", 200.00, "2017-06-27")
budget.add_income("Freelance", 10000.00, "2017-07-10")
budget.current
budget.monthly_report(6,2017)
budget.add_expense("Nintendo Switch", 19000.00, "2017-07-15")
budget.add_expense("Samsung S8 Edge", 39300.00, "2017-07-20")
budget.monthly_report(7,2017)
budget.current